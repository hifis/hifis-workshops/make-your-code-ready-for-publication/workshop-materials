<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.1] - 2020-05-19

### Added

- Added poetry as another example for organizing Python projects to episode 2
- Added links to pandoc and the R-specific documentation tools to episode 3
- Added the SPDX license list and the reuse tool as ways to obtain official license texts to episode 4
- Added hint for contributors to indicate their contributorship and copyright

### Changed

- Improved wording and fixed different typos and formatting issues
- Harmonized order of the `Challenges` section acrross the episodes

## [1.0.0] - 2020-05-05

### Added

- Initial version of the workshop material

[unreleased]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/compare/1.0.1...master
[1.0.1]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/tags/1.0.0
