<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Katrin Leinweber

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 3: Add essential documentation

## Why?

- Otherwise potential users do not want to use or do not know how to use your software.
- Otherwise potential contributors do not know how to provide their contributions in an efficient manner.

## Target Groups and their Documentation Needs

In the following, you can find examples of typical documentation needs for the target groups `user` and `developer`.

- Common documentation needs for both target groups:
  - Problems which the software solves
  - Overview of the software's main features
  - Assumptions and constraints under which the software works
  - Install instructions including required dependencies
  - License under which the software can be used (`LICENSE` file or `LICENSES` folder)
- Documentation needs of `users`:
  - Availability through package managers
  - Basic examples for usage with in- and outputs and example data
  - API documentation in case of libraries
  - How to ask for help in case of problems?
  - Summary of changes in the software's behaviour (`CHANGELOG`)
- Documentation needs of `developers`:
  - How to contribute bug fixes, changes, features (`CONTRIBUTING`)
  - Basic rules for issues, pull/merge requests and communication channels
  - Ground rules for communication behaviour and participation (`CODE_OF_CONDUCT`)
  - Architecture diagrams and decision log
  - Technical concepts, typical solution patterns and extension points
  - Code documentation: API details and "big picture" comments

It is important to think about the relevant target groups for your specific case.
In the workshop example, it is a researchers who want to use your work.
Thus, he/she fits well to the target group `user` with some additional needs such as:
  - Information about how certain results were produced (exact versions of dependencies, operating system etc.)
  - Information about how to cite your software (`CITATION`)

## Typical Documentation Files

In the following, we show typical documentation files and explain their purpose.

- `README`: The front page of your code which you should create in any case.
  - It is directly displayed if someone opens visits your project on platforms such as GitLab and GitHub.
  - It should provide a good overview of all relevant information and link to other files if more detailled information is needed.
- Other typically used documentation files:
  - `CONTRIBUTING`: Explains how to contribute
  - `CODE_OF_CONDUCT`: Explains the ground rules for expected behaviour and participation for contributors
  - `LICENSE` file or `LICENSES` folder: Shows the license(s) under which the material is provided
  - `CHANGELOG`: Explains major changes
  - `CITATION`: Explains how to cite the software in a scientific publication

## Markup Languages

Markup languages are quite popular as they make it easy to write and contribute documentation.
Particularily, platforms such as GitLab and GitHub directly render markup content to HTML and provide further integration features.
In the following, we show typically used markup languages:

- `Markdown` is one of the most popular markup languages.
  It initially focused on Web publishing but there are various extensions and tool chains which make it suitable for creation of complex, technical documentation as well.
  However, the existing dialects and tool chains are a bit fragmented.
  But if you focus on Web publishing it is one of the most used solution today.
- `AsciiDoc` provides an easy to read and write syntax, a rich feature set for creation of complex, technical documentation, as well as a rich ecosystem.
- `ReStructuredText` became popular in the Python community.
  In combination with its documentation generator Sphinx it is well suited for creation of complex, technical documentation.

## Example Project

- [`astronaut-analysis/compare/2-clean-up-code...3-add-docs`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/2-clean-up-code...3-add-docs)
  shows the result of this step:
  - We added a `README.md` file to provide a basic description and usage information.

## Key Points

- Provide documentation for relevant target groups
- Add a README file as a minimum documentation artifact to your repository

## Challenges

- [ ] Please learn more about a markup language (AsciiDoc, RestructuredText) suited for your use case and, particularly, *Markdown*.
- [ ] Please think about the relevant target groups for your code and identify their documentation needs.
  Are they already fulfilled?
  If they are not, please identify improvements, ask for feedback, and start implementing them.
  Please make sure that you have created, at least, a suitable `README` file for your code project.
- [ ] Do you find improvements for the documentation of the example code?
  If you do, please send us a merge request against branch [3-add-docs](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/tree/3-add-docs).

## Further Readings

- Learn more about typically required documentation for Open Source software via:
  - [GitHub's community profiles](https://help.github.com/en/github/building-a-strong-community/about-community-profiles-for-public-repositories)
  - [Open Source Guides](https://opensource.guide/)
- Learn more about good `README` files:
  - [Typical, minimal README.md structure](https://github.com/RichardLitt/standard-readme)
  - [Curated list of awesome READMEs](https://github.com/matiassingers/awesome-readme)
  - [One Sentence per Line Principle](https://rhodesmill.org/brandon/2012/one-sentence-per-line/)
- Learn more about Markdown:
  - [10min interactive tutorial](https://commonmark.org/help/tutorial/)
  - [Cheatsheet](https://commonmark.org/help/)
  - [Support in GitLab](https://docs.gitlab.com/ee/user/markdown.html)
  - [Support in GitHub](https://guides.github.com/features/mastering-markdown/)
- Learn about further markup languages and documentation generators:
  - The [AsciiDoc Writer's Guide](https://asciidoctor.org/docs/asciidoc-writers-guide/) provides a gentle introduction to AsciiDoc.
  - The [Quick Sphinx Tutorial](https://quick-sphinx-tutorial.readthedocs.io/en/latest/rst.html) provides a good introduction to reStructuredText.
  - You can generate documentation from your R function comments via [roxygen](https://roxygen2.r-lib.org/) and create project Web sites via [pkgdown](https://pkgdown.r-lib.org/).
  - You can use [Pandoc: a universal document converter](https://pandoc.org/) to convert between the different formats.
