<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 4: Add a license

> **IMPORTANT:** This information is no legal advice and solely reflects the experiences of the episode contributors.
> Please contact a lawyer or your organizational legal department, if you are in doubt and require solid legal advice.

## Why?

- Otherwise potential users cannot (re-)use your software from the legal point of view.

## Copyright

- Software is protected by copyright.
- Copyright protects the expression of an idea but not the idea itself.
  In context of software, it includes source code, object code, and design material (§ 69a UrhG, German copyright law).
- Copyright gives the copyright holder certain exclusive rights such as usage, creation of copies, distribution, creation of derivative works (§ 69c UrhG, German copyright law).
- In accordance to German Copyright law, some of the exclusive rights (e.g., attribution) cannot be waived or transferred to a third-party.
- Copyright gives other persons no rights, unless the copyright holder explicitly grants them.
  The copyright holder can decide in what way his/her work is used or not used at all.
- Copyright law works differently depending on your country.
  In the following, we orientate on **German copyright law**.

### What is protected by copyright?

- Software as source code, object code, or integrated with hardware including documentation and accompanying material.
- In which case is it protected by copyright?
  - A certain threshold of originality is required.
  - If two developers independently create the same source code, the required threshold of originality might be not achieved.

### Who is the copyright holder of the software?

- The copyright holder of a software is the person or organization who has the exclusive rights with regard to this software.
- Everyone who contributed to the creation of the software ("authors") is considered copyright holder of the software.
  The quality and size of the contribution do not matter.
- Usually, authors and copyright holders are the same.
  However, if an author is paid by an employer, the employer is often the copyright holder instead of the author and obtains most of the exclusive rights (see below).
- All copyright holders jointly exercise the rights granted by copyright.
  For example, this aspect is relevant, if you want to re-license a software.
  In such a case, all copyright holders have to agree on the license change.

### What if I developed the software as part of my job?

- If you are paid by an employer to create the software, the employer obtains most of the exclusive rights and is considered the copyright holder.
  The employer is entitled to exercise all economic rights in the software, unless otherwise agreed (§ 69b UrhG, German copyright law, "Verwertungsrechte").
- You are still the author and keep certain rights such as attribution (§§ 12 - 14 UrhG, German copyright law, "Urheberpersönlichkeitsrechte").
- In the following, we use the term `copyright holder` to indicate the person or organization that is entitled to exercise all economic rights in the software ("Verwertungsrechte").

### What if I developed the software on my own initiative?

- If your private initiative is the reason for the development of the software and you have developed the software off work,
  you are the sole copyright holder.
  This is also the case, if you have been inspired by your work experiences and knowledge.

## Software Licenses

- Software licenses are a way for a copyright holder to grant rights to other persons or legal entities.
- A software license grants certain rights (e.g., use, copy, distribute) and demands certain obligations (e.g., disclosure of source code under a certain license, constraints concerning the distribution, attribution).
- Every software that you use has to be covered by a license.
- Your users are covered by whatever license you place on software that you write.
- License incompatibility exists when a program is a derivative work of components licensed under conflicting licensing terms.

### Proprietary Software Licenses

- Non-standard licenses with specific licensing terms created by a specific copyright holder. 
- Examples: Commercial License, Shareware License, Freeware License

### Free and Open Source Software Licenses

- They are defined by the Free Software Foundation (FSF) and the Open Source Initiative (OSI).
- Both terms essentially refer to the same set of licenses but favor different values (development methodology vs. social movement).
- In general, free software and open source software licenses imply the availability of the source code to the users and allow open distribution, modification, and re-use of the code.

#### Copyleft ("The world is evil.")

- Requires you to use the same license (or a compatible license for your source code) as soon as the distributed work is a derivative of the covered work.
- The definition of a "derivative work" differs from license to license.
- Strong ("No exceptions!") vs. weak ("exceptions possible") copyleft licenses exist.
- Examples: GNU General Public License (GPL), Eclipse Public License (EPL)

#### Permissive ("The world is good.")

- Generally, these licenses are compatible and interoperable with most other licenses.
- These licenses permit using the code in non-free and proprietary derivative works.
- Place very few restrictions on what can be done with the code.
- Examples: BSD-style, MIT

### Public Domain

- Authors waive all their exclusive rights.
- Not possible in accordance to German copyright law ("Urheberpersönlichkeitsrechte").
- Effectively "no copyright" in accordance to US-American copyright law.

## Minimal License Checklist

In the following, you can find a short checklist for cases similar to the workshop example.
The checklist is aimed for cases in which:
- You created the software as part of your job.
- You created the software completely on your own.

### 1. Choose a license

- Decision depends on different factors such as your publication strategy, restrictions from third-party dependencies, restrictions in your working group/organization, typically used licenses in your community of practice.
- In general, please **only use** OSI-approved licenses for your own code.
- We encourage you to use permissive licenses because they make it easier to re-use code:
  - Small works: MIT or BSD (3-clause, 2-clause) license, licenses are short and quite understandable
  - Larger works: Apache License 2.0, a bit more complicated but allow to practice patent claims of contributors to the code

Please make sure that you understand the rights and obligations resulting from licenses of third-party packages.
For example, GPL (version 3) requires that the source code of all linked code is disclosed using a GPL (version 3) compatible license.
As a basis, list your dependencies including their license and the way they are used (e.g., statically linked, dynamically linked, run as command).
Then, you are able to identify possible incompatibilities, obtained rights, and obligations that have to be fulfilled.
Please ask someone with knowledge about software licenses to support you in this analysis.

### 2. Ask your boss for permission to share your software using the selected license

- Your organization holds the rights on software that you produce during your work ("Verwertungsrechte").
  For that reason, your organization has to explicitly permit sharing the software under the selected license.
- The concrete details depend on your organization.

### 3. Prepare your code

The very minimum requirement is that you add a file named `LICENSE` which contains the text of your selected license to your repository.
In addition, you should clearly state the copyright holder(s).
We recommend to add this information in a standardized way following the REUSE 3 specification.

- a) Add licenses to the repository
  - The REUSE approach allows you to indicate multiple licenses that are used in your repository in a standardized way.
    For example, a dataset uses another license than your code.
  - Create a directory `LICENSES` and add the used software license texts there.
  - Make sure that you keep the license texts as they are.
    However, in some cases, the license text template contains a copyright hint which you have to adjust.
- b) Provide license and copyright information for all files
  - The REUSE approach makes sure that all required information (e.g., license link, copyright statements) is contained in your repository.
  - A linter tool is provided to automate compliance checks.
  - Unfortunately, there is no built-in support in platforms such as GitLab and GitHub right now.
- c) Provide a license and copyright holder hint
  - We recommend to add a brief `License` section to your `README` file which sums up the main license(s) and indicates the copyright holder ("Verwertungsrechte").
  - The concrete details of the copyright holder statement may depend on your organizational policies.
- d) List the dependencies and their licenses
  - The list helps users to find out about those licenses easily.
  - In addition, it helps you to make sure that you checked the licenses of third-party dependencies accordingly. 

## Example Project

- [`astronaut-analysis/compare/3-add-docs...4-add-license-infos`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/3-add-docs...4-add-license-infos)
  shows the result of this step:
  - Indicated license of third-party dependencies
  - Recorded license and copyright information following the REUSE specification
  - Added a license and copyright holder hint
  - Added `reuse` as an additional linter

## Key Points

- **Minimum:** Add a license file and state the copyright holder
- **Recommended:** Follow REUSE Specification Version 3.0.0
- Consider third-party licenses from the very beginning

## Challenges

### During the Workshop

- [ ] Please find out about the licenses of third-party dependencies in your code.
  List all dependencies including their license and the way they are used.
- [ ] Please decide which license is suitable for your code.
  Does the license fit with licenses of your identified dependencies?
- [ ] Please prepare your code following the recommendations that have been outlined in this episode.
  At least, add a license file and state the copyright holder(s).

### After the Workshop

- [ ] Please find out whom in your organization you should ask for permission to share your code under a specific license.
- [ ] Please find out about license and copyright specific rules and recommendations in your organization.

## Further Readings

- [German copyright law](http://www.gesetze-im-internet.de/urhg/)
- [Open Source Definition](https://opensource.org/osd)
- [Free Software Definition](https://www.gnu.org/philosophy/free-sw.html)
- ["A Quick Guide to Software Licensing for the Scientist-Programmer" section "Compatibility, Proliferation, Fragmentation, and Directionality"](https://doi.org/10.1371/journal.pcbi.1002598)
- [Choose an open source license](https://choosealicense.com/)
- [OSI approved licenses sorted by category](https://opensource.org/licenses/category)
- You can download licenses from the [SPDX License List](https://spdx.org/licenses/) manually or via the [REUSE Tool](https://github.com/fsfe/reuse-tool)
  (e.g., `reuse download MIT` downloads the MIT license into the `LICENSES` directory).  
- [Overview about software licenses including granted right and obligations](https://tldrlegal.com/)
- Reuse: [Tutorial](https://reuse.software/tutorial/), [FAQ](https://reuse.software/faq/), [Specification](https://reuse.software/spec/)
