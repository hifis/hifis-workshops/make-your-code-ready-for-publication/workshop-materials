<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 5: Make your code citable

## Why?

- Software is a research product, just like a paper or a monograph.
- Creating and maintaining research software is academic work, and should allow for academic credit and careers.
- Citing software is an important part of the provenance of research results and enables reproducibility.

### Digitalization of Research has made Software a central Asset of Research

- Creation and maintenance of research software requires knowledge and research.
  Research software work is therefore academic work and it should be treated on par with papers and books.
- In the current system, credit is provided for publication and (re-) use of academic work.
  Academic careers are (partly) built on such credit.
- The common way to award credit is citation of work that is used in or built upon one's own work.

### Software Citation enables Reproducibility of Research Results

- The provenance record of a research result should include the software that has been used to achieve the result.
- Correct software citation makes it possible to reproduce and evaluate the research result as well as to find and evaluate the research software itself.

## How to cite Software?

- Please make sure that you cite all software packages (including your own) in the reference list of your academic work.
- Please follow the guidelines outlined in [Research software citation for researchers](https://research-software.org/citation/researchers/).
  These guidelines provide hints where to look for relevant information and how to cope with incomplete citation metadata.


In the following example, we cite the developed analysis software stating the **authors**, the **year of publication**, its **title**, its **version** and its **digital object identifier** (DOI):

> "The data sets and the notebook containing the analysis details have been published separately [11]."
>
> ### References
> - ...
> - [11] Schlauch, Tobias & Haupt, Carina. (2019). Analysis of the DLR Knowledge Exchange Workshop Series on Software Engineering (Version 1.2.0). Zenodo. https://doi.org/10.5281/zenodo.3403991

## How to make your Software citable?

Please make sure that other persons can easily cite your software by:
  - Providing citation metadata,
  - Archiving your software and obtaining a persistent identifier (PID) for it as well as
  - Providing a prominent citation hint as part of your documentation.

### Providing Citation Metadata

- You can directly manage citation metadata as part of your source code repository by
  providing a file [`codemeta.json`](https://codemeta.github.io/) (machine-readable information about your software including citation metadata) **or**
  providing a file [`CITATION.cff`](https://citation-file-format.github.io) (human/machine-readable citation metadata).
- Another option is to let digital object repositories such as Zenodo manage your citation metadata.
- Finally, you could also combine both approaches.

#### Authorship and Contributorship in Software

When determining the authors, please consider the following recommendations:

- There are no universally accepted **guidelines** for software authorship
- Different **roles** than `programmers` might be considered as the authors of a software.
  For example, testers, reviewers, technical writers, maintainers, release engineers, software architects, UX designers, etc., may all qualify for authorship.
- **Decisions** about authorship are project-specific, but must follow good scientific practice.
  Refer to the [ICMJE Uniform Requirements](http://www.icmje.org/recommendations/browse/roles-and-responsibilities/defining-the-role-of-authors-and-contributors.html) for papers, and translate the best practice to software, e.g.
  - There may be *no* honorary authorship.
  - The contribution to the software must be substantial.
  - Final approval of the outcome may be covered by a CLA.
  - Agreement to be accountable for - at least the own - changes may be factored into the decision.
- **Contributors** should be acknowledged, but are not software authors.
  Typical examples include issue reporters, typo fixers, evangelists promoting the software, and managers/PIs with no substantial contribution to the software itself.
  Consider using automation to keep track of contributors, for example, with the help of the [All Contributors bot](https://allcontributors.org).

### Archiving Software

- Archiving your software is crucial to ensure long-term availability.
  In comparison, source code repository URLs break easily or the software may become unavailable at all.
- Such an archive allows you to deposit your software and to obtain a PID (e.g., a DOI).
- You can use the obtained PID to persistently reference your software in a research publication.

#### Example 1: Zenodo

[Zenodo](https://zenodo.org/) is a well-known example of a general-purpose digital object repository.
It allows you to archive specific versions of your software, manages your citation metadata, and it allows you to obtain DOIs to reference the different software versions.
Zenodo also provides an integration with GitHub and supports versioned DOIs which help you to efficiently manage the different versions of your software deposit on Zenodo.

#### Example 2: Software Heritage

[Software Heritage](https://www.softwareheritage.org/) is a dedicated source code archive.
It already archives many publicly available source code repositories and automatically assigns them a PID.
You can also pro-actively [save your code in Software Heritage](https://www.softwareheritage.org/save-and-reference-research-software/).
However, Software Heritage does not directly support citation metadata.
For that reason, you should add citation metadata to your source code repository.

## Software Journals

In addition, you may still consider writing a software paper.
Particularly, if your organization only "awards" peer-reviewed publications.
Specifically, we recommend to publish your software paper in the [Journal of Open Source Software](https://joss.theoj.org/) (JOSS) because of its developer-friendly approach.
In addition, the JOSS process ensures that your software itself is citable as recommended in this episode.

## Example Project

- [`astronaut-analysis/compare/6-prepare-for-citation`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/4-add-license-infos...5-prepare-citation)
  shows the result of this step:
  - Added a citation hint
  - Added list of main contributors
  - Citation metadata and archiving is handled by Zenodo

## Key Points

- Cite all relevant software packages as good as possible in your academic work
- Make your code citable by adding citation metadata and archiving your software
- Encourage citation of your software

## Challenges

- [ ] Please experiment with citation metadata files for your code by following [the examples on the Research Software Citation website](https://research-software.org/citation/developers/).
- [ ] Please find out more about suitable software journals for your domain.
- [ ] Please find out whether your organization provides recommendations with regard to software citation.
- [ ] Please select a suitable approach for your code and make your code citable.
  Please make sure to add a prominent citation hint to your documentation.

## Further Readings

- [Research Software Citation](https://research-software.org/citation/)
- Chue Hong, Neil. (2019, May). How to cite software: current best practice. Zenodo. https://doi.org/10.5281/zenodo.2842910
- [CodeMeta Generator](https://codemeta.github.io/codemeta-generator/)
- [CFF Generator](https://citation-file-format.github.io/cff-initializer-javascript/)
- [Overview of Software Journals](https://www.software.ac.uk/which-journals-should-i-publish-my-software)
