<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Katrin Leinweber

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 6: Release your code

## Why?

- Otherwise users do not know which version is considered stable.
- Otherwise users do not exactly know which version has been used to produce a specific result.
- You are able to easier start investigating problems that a user had with your code.

## Release Basics

- A *release* is a specific working version of your software which you used, for example, to produce specific research results.
- The *release number* uniquely identifies this specific released software version.
  There are different *release number schemes* such as [Semantic Versioning](https://semver.org/) (e.g., `1.0.1`) and [Calendar Versioning](https://calver.org/) (e.g., `2021-04-22`).
- A *release tag* is used to mark this specific released software version in your source code repository.
  It should be named in accordance to the release number to ensure that it can be easily found if needed.
- The *changelog* documents all released versions of your software including the major changes of these versions.
  It is written from the user`s point of view.
- The *release package* can be used by the user to directly install and use this specific released software version.
  It contains the software (e.g., source code, compiled executable) including proper user documentation (e.g., install and usage instructions, license information, citation information).
  In the simplest form it might be a snapshot of your source code repository packaged as [ZIP file](https://en.wikipedia.org/wiki/ZIP_(file_format)).
  Details depend on your programming language and target audience.
  For example, for a standalone software tool, you would consider publishing it on a code distribution platform such as [PyPi](https://pypi.org/), [CRAN](https://cran.r-project.org/) or [Maven Central](https://mvnrepository.com/repos/central).
  On this basis, users can easily install and use it via their typically used package manager.

## Minimal Release Checklist for Research Code

Before your start, please make sure that you:

- Defined which release number scheme you want to use.
- Established the name mapping between release number and release tags.
- Defined how you handle citation metadata (see also [episode 5](05_make-your-code-citable.md#providing-citation-metadata)).
- Defined where you want to archive your code (see also [episode 5](05_make-your-code-citable.md#archiving-software)).
- Defined the format and content of your release package(s).

### 1. Prepare your code for release

- Define the release number for the release.
- Document the user-visible changes in the changelog.
- Update the citation metadata for this release.

### 2. Check your code

- Make sure that your code fulfills its intended functionality in the environments for which your code should work.
  You can check it manually, although it is recommended to automate some checks by using automated tests and linting (see also [episode 2](02_make-sure-that-your-code-is-in-a-sharable-state.md)).
- Review your documentation.

### 3. Publish and archive the release

- Mark the release in the source code repository using a tag.
  This step makes sure that you can easily identify the content of the released version in the source code repository.
- Create the release package(s) and eventually publish it on a code distribution platform.
- Archive the release package(s).

## Example Project

- [`astronaut-analysis/compare/5-prepare-for-release`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/5-prepare-citation...6-release-code)
  shows the result of this step:
  - Added a changelog
  - Marked the released version in the Git repository using the tag `1.0.0`
  - Published the tagged software version (source code ZIP package) in the corresponding Zenodo deposit

## Key Points

- Mark used, working software versions as releases using release numbers and tags
- Document important changes in a changelog
- Archive the release package

## Challenges

- [ ] Please find out more about the different release number schemes and select a scheme for your code.
- [ ] Please find out more about writing proper changelogs and create a suitable changelog for your code.
- [ ] Please find out more about release-specific support in GitLab and GitHub.
- [ ] Please learn more about Zenodo and its GitHub integration by following [the Open Science MOOC tutorial](https://github.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/blob/master/content_development/MAIN.md#using-github-and-zenodo-).
- [ ] Please learn more about Software Heritage and follow their [Save and reference research software](https://www.softwareheritage.org/save-and-reference-research-software/) tutorial.

## Further Readings

- [Git Tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
- [keep a changelog](https://keepachangelog.com/)
