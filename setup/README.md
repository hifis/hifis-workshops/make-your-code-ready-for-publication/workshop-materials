<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

## Workshop Setup

Here you find basic information about the workshop setup.

## Plan the Schedule

We use the following schedule for an online setup of the workshop.
Please modify times and dates according to your needs.

## Day 1 - (DD.MM.YYYY)
- 09:00-09:30 Welcome & Introduction
- 09:30-11:00 Theory Part 1
- *11:00-11:15 Break*
- 11:15-12:45 Practice Part 1
- 12:45-13:00 Wrap Up & Feedback Day 1

## Day 2 - (DD.MM.YYYY)
- 09:00-09:30 Welcome & Introduction
- 09:30-11:00 Theory Part 2
- *11:00-11:15 Break*
- 11:15-12:45 Practice Part 2
- 12:45-13:00 Wrap Up & Feedback Day 2


## Setup the Pads

The directory [pads](pads) contains pad templates that we usually set up in a service such as [HedgeDoc](https://hedgedoc.org/):
- The [Main Pad](./setup/pads/main.md) is used for presentation.
- [Practice Pad](./setup/pads/practice.md) is used to organize the practice phases.

Please copy and modify them to your needs.
Content to be changed is indicated by `[setup]`.
