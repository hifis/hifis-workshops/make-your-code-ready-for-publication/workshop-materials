<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
This markdown file is a template for your practice pad. Please copy and modify the pad.
Content to be changed in the source is indicated by [setup].
-->



[setup]: # (add workshop date)
# Practice Phases: Let's Make Your Script Ready for Publication (DD.-DD.MM.YYYY)

- You work on self-chosen tasks. Please think about the topics you want to work on during the theory blocks.
- At the beginning of each practical block, please briefly state which tasks you want to work on.
- After the practical block, please give a short feedback to the group about your results.
- If you have any questions / need for discussion, you can always contact the instructors.

[setup]: # (insert link to main pad)
> Back to the [Main Pad](main.md).

## On which tasks do you want to work today?
> Please write down below on which aspect you want to work today.



[setup]: # (change time according to your schedule)
## Practice Phase Day 1 - Until HH:MM

Here are some ideas on which aspects you could work today.

### Put your code under version control

- [ ] Please **put your code into a Git repository**.
- [ ] Please **learn more about [Git LFS](https://git-lfs.github.com/)** ([GitLab support](https://docs.gitlab.com/ee/topics/git/lfs/) / [GitHub support](https://help.github.com/en/github/managing-large-files/configuring-git-large-file-storage)) or the other mentioned approaches to handle large data.
- [ ] Please think about the artifacts in your repository. Is there any room for improvements? If possible, please implement the identified improvements and **clean up your repository** accordingly.
- [ ] If you have not already done it, please **create a suitable `.gitignore` file** (https://gitignore.io/) and add it to your repository. You can remove unwanted files using [git filter-branch](https://git-scm.com/docs/git-filter-branch) or [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/). But be aware that these tools change your Git repository history!

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/01_put-your-code-under-version-control.md#challenges) for further details.

### Cleaning up code

- [ ] Please find out about **code structuring recommendations, style guides and tools** for your domain.
- [ ] Please **identify improvements for your code**, ask for feedback, and start implementing them.
- [ ] Do you **find further code improvements in the example code**? If you do, please send us a merge request against branch 6-prepare-for-citation.

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/02_make-sure-that-your-code-is-in-a-sharable-state.md#challenges) for further details.

### Add essential documentation
- [ ] Please **learn more about a markup language** (AsciiDoc, RestructuredText) suited for your use case and, particularly, Markdown.
- [ ] Please think about the **relevant target groups** for your code and **identify their documentation needs**. Are they already fulfilled? If they are not, please identify improvements, ask for feedback, and start implementing them.
- [ ] Please make sure that you have created, at least, a suitable **README file for your code project**.
- [ ] Do you find **improvements for the documentation of the example code**? If you do, please send us a merge request.

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/03_add-essential-documentation.md#challenges) for further details.




[setup]: # (change time according to your schedule)
## Practice Phase Day 2 - Until HH:MM

Here are some ideas on which aspects you could work today. But it is totally fine to continue with yersterday`s tasks.

### Add a license

- [ ] Please **find out about the licenses of third-party dependencies** in your code. List all dependencies including their license and the way they are used.
- [ ] Please decide which **license is suitable for your code**. Does the license fit with licenses of your identified dependencies?
- [ ] Please prepare your code following the recommendations that have been outlined in this episode. At least, **add a license file** and state the **copyright holder(s)**.

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/04_add-license.md#challenges) for further details.

### Make your code citable

- [ ] Please **experiment with citation metadata files** for your code by following [the examples on the Research Software Citation website](https://research-software.org/citation/developers/).
- [ ] Please find out more about **suitable software journals for your domain**.
- [ ] Please find out whether your organization provides **recommendations with regard to software citation**.
- [ ] Please select a **suitable approach for your code and make your code citable**.
- [ ] Please make sure to add a **prominent citation hint to your documentation**.

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/05_make-your-code-citable.md#challenges) for further details.

### Release your code

- [ ] Please find out more about the different **release number schemes** and **select a scheme** for your code.
- [ ] Please find out more about writing proper changelogs and **create a suitable changelog** (https://keepachangelog.com/) for your code.
- [ ] Please find out more about **release-specific support in [GitLab](https://docs.gitlab.com/ee/user/project/releases/) and [GitHub](https://docs.github.com/en/repositories/releasing-projects-on-github/managing-releases-in-a-repository)**.
- [ ] Please learn more about **Zenodo and its GitHub integration** by following [the Open Science MOOC tutorial](https://github.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/blob/master/content_development/MAIN.md#using-github-and-zenodo-).
- [ ] Please learn more about **Software Heritage** and follow their [Save and reference research software](https://www.softwareheritage.org/save-and-reference-research-software/) tutorial.

> Please see the [Workshop Materials](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/06_release-your-code.md#challenges) for further details.

